# check the drawio diagram


variables:
    IP_READ_MAIN: 95.216.159.187
    IP_READ_SECONDARY: 96.216.159.187
    IP_WRITE: 65.108.252.132
    IP_BALANCER: 66.108.252.132
    REGISTRY_PORT: 5000
    REGISTRY: ${IP_BALANCER}:${REGISTRY_PORT}
    # NGINX servicer for handling incoming queries and responses and for rerouting the trafic between READ nodes
    PORT_NGINX: 8080
    # READ swarm service on the READ node
    PORT_MATCHING_SERVICE_READ: 5011
    # WRITE swarm communication service on the WRITE node
    PORT_WRITE_SERVICE_COMMUNICATION: 6011
    # WRITE swarm model update service on the WRITE_NODE
    PORT_WRITE_SERVICE_MODEL_UPDATE: 6012
    # DB engine connected to the DISK on the WRITE node - stores index, matching, 
    PORT_COLD_STORAGE: 5432

# READ and WRITE nodes are similar in some functionality. WRITE can under some conditions used by the BALANCER as the READ node in case both READ nodes failed.
# Both READ_COMMUNICATION and WRITE_COMMUNICATION services have replicas of Flask services with common flask service handles:
#  - /embed: query -> embedding
#  - /find_neighbor_centroids : embedding, n_centroids -> centroid_ids
#  - /match_with_index: embedding, centroid_id, n_closest -> list_of_matched_docs
#  In order not to repeat the work of READ_MAIN, WRITE node receive preprocessed query with its embedding directly from READ_MAIN.
#  WRITE node also contains cold storage and multiple replicas to handle the finetuning of 1) embedder, 2) clustering, 3) matching models.
#  Because of the high memory load:
#  - either WRITE MODEL_UPDATE replicas can be partitioned on several machines and optimize models in distributed manner https://arxiv.org/ftp/arxiv/papers/1802/1802.00304.pdf
#  - or a more powerful cluster can be assembled for WRITE node in order to update models and store all the indices on one machine
#  Each MODEL_UPDATE Flask service has the following handles (Embedder, Clustering, Matching models hold all parameters, so only new ones are passed):
#      - /update_embedder new_queries
#      - /update_clustering new_queries
#      - /update_matching new_queries, new_labels


stages:          # List of stages for jobs, and their order of execution
  - build
  - deploy
  - batch-update


build-balancer-job:
  stage: build
  script:
    - echo "Initializing unsequred registry if it is not running yet... `docker run -p ${REGISTRY_PORT}:${REGISTRY_PORT} registry:2 -it registry `"
    - echo "Building docker image for BALANCER node based on NGINX in registry - ${REGISTRY}"

build-read-job:
  stage: build
  script:
    - echo "Building docker image for READ node in registry - ${REGISTRY}"

build-write-job:
  stage: build
  script:
    - echo "Building docker image for WRITE node in registry - ${REGISTRY}"

deploy-balancer-job:
  stage: deploy
  environment: production
  script:
    - echo "Deploying balancer on ${IP_BALANCER}..."
    - echo "Application successfully deployed."

deploy-write-job:
  stage: deploy  
  environment: production
  script:
    - echo "Deploying WRITE node..."
    - echo "Starting the swarm service for WRITE_COMMUNICATION"
    - echo "Starting the swarm service for WRITE_MODEL_UPDATE"
    - echo "Enabling the retrain trigger on the WRITE cluster - pull the trigger to initiate the retraining"
    - echo "Application successfully deployed."

deploy-read-job:
  stage: deploy  
  environment: production
  script:
    - echo "Deploying READ node..."
    - echo "Starting the swarm service for READ_SECONDARY with initialization from WRITE"
    - echo "Switching READ_SECONDARY -> READ_MAIN"
    - echo "Nginx switch to the new Main READ, downgrading the old READ_MAIN -> READ_SECONDARY."
    - echo "Starting the swarm service for READ_SECONDARY with initialization from WRITE"
    - echo "Application successfully deployed."

init-write-job: # This job runs can be considered as the initialization
  stage: batch-update
  script:
    - echo "Checking if the WRITE cluster is up and running, if not -> Initialize WRITE cluster from cold storage"

batch-update-job: # This job initializes 
  stage: batch-update
  usage: "trigger activated"
  script:
    - echo "Updating Secondary READ cluster from WRITE cluster..."
    - echo "Updating 1) clustering centroids & partitions, 2) updating indices of the partitions, 3) updating matching model..."
    - echo "Granting Secondary READ cluster Main READ status"
    - echo "Nginx switch to the new Main READ, downgrading the old Main Read to Secondary Read."
