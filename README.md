# HardML Deploy Final Project

My telegram is the same as the gitlab username @ivanzhovannik

## General description of the problem

The problem and the solution are described in the diagram - `final_project_template.drawio`

## Solution description

First of all, let us start with the generic high-level user-service interaction. Figure 1 explains this interaction:

![Figure 1](https://gitlab.com/ivanzhovannik/hardml-deploy-final-project/-/raw/main/production_inference_scheme.png)


